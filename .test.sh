#!/bin/bash

clear

DIR=../../ELaSTIC-`cat VERSION`-bin

mkdir -p build/
rm -rf build/*
cd build/
cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR -DPARASAIL_LIB_DIR=/opt/parasail-1.2-gnu/lib/ -DPARASAIL_INCLUDE_DIR=/opt/parasail-1.2-gnu/include/
make -j4 install

/***
 *  $Id$
 **
 *  File: read_edges.hpp
 *  Created: Mar 13, 2017
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE_MIT.txt.
 *
 *  This file is part of ELaSTIC.
 */

#ifndef READ_EDGES_HPP
#define READ_EDGES_HPP

#include <sstream>

#include <jaz/boost/files.hpp>
#include <mpix2/read_lines.hpp>

#include "SequenceDB.hpp"
#include "config_log.hpp"
#include "iomanip.hpp"


inline bool extract_edges(int n, const std::vector<std::string>& ln, std::vector<read_pair>& edges) {
    int s = 0;
    int t = 0;

    std::istringstream is;

    for (int i = 0; i < ln.size(); ++i) {
        is.str(ln[i]);
        is.clear();

        is >> s >> t;

        if (!is) return false;
        if (s == t) return false;
        if ((s >= n) || (t >= n)) return false;

        edges.push_back(make_read_pair(s, t));
    } // for i

    return true;
} // extract_edges

inline std::pair<bool, std::string> read_edges(const AppConfig& opt, AppLog& log, Reporter& report, MPI_Comm comm,
                                               const SequenceList& SL, std::vector<read_pair>& edges) {
    report << step << "reading candidate edges..." << std::endl;

    int rank;
    MPI_Comm_rank(comm, &rank);

    std::vector<fs::path> files;

    double t0, t1;
    t0 = MPI_Wtime();

    if (jaz::files(opt.edges, std::back_inserter(files)) == false) {
        return std::make_pair(false, "unable to scan " + opt.input);
    }

    if (files.empty() == true) return std::make_pair(false, "no edge files to process");

    report << info << "found " << files.size() << " file(s)..." << std::endl;

    std::vector<std::string> ln;

    for (int i = 0; i < files.size(); ++i) {
        if (fs::is_regular_file(files[i]) == false) continue;

        bool res = mpix::read_lines(files[i].string(), ln, comm);
        if (res == false) return std::make_pair(false, "could not read edges");

        res = extract_edges(SL.N, ln, edges);
        if (res == false) return std::make_pair(false, "could not parse edges");
    } // for i

    t1 = MPI_Wtime();
    if (rank == opt.dbg) report.stream << debug << "time: " << (t1 - t0) << std::endl;

    return std::make_pair(true, "");
} // read_edges

#endif // READ_EDGES_HPP

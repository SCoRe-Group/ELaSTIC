/***
 *  $Id$
 **
 *  File: create_smatrix.hpp
 *  Created: Jun 20, 2013
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2013 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE_MIT.txt.
 *
 *  This file is part of ELaSTIC.
 */

#ifndef CREATE_SMATRIX_HPP
#define CREATE_SMATRIX_HPP

#include <string>
#include <vector>

#include <boost/lexical_cast.hpp>
#include <jaz/string.hpp>

#include <parasail.h>


inline boost::tuple<const parasail_matrix_t*, bool>
create_smatrix(std::string val, bool is_dna, int& m, int& s, int& g, int& h) {
    val.erase(val.begin());
    val.erase(val.end() - 1);

    std::vector<std::string> agap;
    jaz::split(',', val, std::back_inserter(agap));

    const parasail_matrix_t* M = 0;
    bool A = true;

    try {
        if (agap.size() == 3) {
            g = boost::lexical_cast<int>(agap[1]);
            h = boost::lexical_cast<int>(agap[2]);

            M = parasail_matrix_lookup(agap[0].c_str());
            A = true;
        } else if (agap.size() == 4) {
            int gaps[4];
            for (unsigned int i = 0; i < 4; ++i) gaps[i] = boost::lexical_cast<int>(agap[i]);

            m = gaps[0];
            s = gaps[1];
            g = gaps[2];
            h = gaps[3];

            if (is_dna == true) {
                M = parasail_matrix_create("ACGT", m, s);
                A = false;
            }
        } else return boost::tuple<const parasail_matrix_t*, bool>(0, true);
    } catch (boost::bad_lexical_cast& ex) {
        return boost::tuple<const parasail_matrix_t*, bool>(0, true);
    }

    return boost::tuple<const parasail_matrix_t*, bool>(M, A);
} // create_smatrix

#endif // CREATE_SMATRIX_HPP

#!/bin/bash

mkdir -p build/
rm -rf build/*
cd build/
cmake ../ -DPARASAIL_LIB_DIR=/opt/parasail-1.2-gnu/lib/ -DPARASAIL_INCLUDE_DIR=/opt/parasail-1.2-gnu/include/
make
src/mpi/elastic-sketch > ../doc/elastic-sketch.txt
cd ..
rm -rf build/*
DIR=../ELaSTIC-`cat VERSION`
mkdir -p $DIR
cp -R * $DIR/

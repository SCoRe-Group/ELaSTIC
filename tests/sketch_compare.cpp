#include "SequenceCodec.hpp"
#include "SequenceDB.hpp"
#include "config_log.hpp"
#include "iomanip.hpp"
#include "create_smatrix.hpp"

#include "bio/fastx_iterator.hpp"
#include "bio/sequence_compare.hpp"
#include "jaz/hash.hpp"
#include "jaz/string.hpp"



template <typename Hash>
void shingles(const AppConfig& opt, const std::string& s, Hash h, std::vector<uint64_t>& sh) {
    int l = s.size();
    int k = opt.kmer;

    SequenceCodec sc(opt.is_dna);
    sh.resize(l - k + 1);

    for (int i = 0; i < l - k + 1; ++i) {
	sh[i] = h(sc.code(s.substr(i, k)));
	// sh[i] = h(s.substr(i, k));
    }
} // shingles


// XORshift on shingles
void xorsketches (std::vector<uint64_t>& shingles, int NF, std::vector<uint64_t>& sketch) {
    sketch.resize(NF);
    for (int i = 0; i < NF; ++i) {
        for (int s = 0; s < shingles.size(); ++s) {
            shingles[s] = jaz::xorshift64star(shingles[s]);
        }
        sketch[i] = *std::min_element(shingles.begin(), shingles.end());
    }
    std::sort(sketch.begin(), sketch.end());
} // xorsketches


void sketches(const AppConfig& opt, std::vector<uint64_t>& shingles, int NF, std::vector<uint64_t>& sketch) {
    sketch.clear();
    for (int i = 0; i < shingles.size(); ++i) {
	if (shingles[i] % opt.mod == NF) sketch.push_back(shingles[i]);
    }
    std::sort(sketch.begin(), sketch.end());
} // sketches


template <typename Ostream>
void process(AppConfig opt, const SequenceList& SL, Ostream& of) {
    jaz::murmur264 H;

    // kmer vectors
    // contains all shingles from sketches in vectors after repeated hashing
    std::vector<uint64_t> shingles0;
    std::vector<uint64_t> shingles1;

    // sketch vectors
    // contains all hashed sketches
    std::vector<uint64_t> sketch0;
    std::vector<uint64_t> sketch1;

    // number of sequences, then number of pairs
    int n = SL.seqs.size();
    n >>= 1;

    // compute semi global alignment
    int g = 0;
    int h = 0;
    bio::scoring_matrix sm;
    create_smatrix(opt.gaps, opt.is_dna, sm, g, h);

    bio::semi_global_alignment sgalign(sm, g, h);

    // iterate through sequece strings and ID all kmers
    for (int i = 0; i < n; ++i) {
	const std::string& s0 = SL.seqs[2 * i].s;
	const std::string& s1 = SL.seqs[2 * i + 1].s;

	if ((s0.size() < opt.kmer) || (s1.size() < opt.kmer)) continue;

        // generate shingles for input sequences
        shingles(opt, s0, H, shingles0);
        shingles(opt, s1, H, shingles1);

        // calculate containment
        sketches(opt, shingles0, 0, sketch0);
        sketches(opt, shingles1, 0, sketch1);

        int is = jaz::intersection_size(sketch0.begin(), sketch0.end(), sketch1.begin(), sketch1.end());
        double cont = (1.0 * is) / std::min(sketch0.size(), sketch1.size());

        // calculate jaccard
        xorsketches(shingles0, opt.cmax, sketch0);
        xorsketches(shingles1, opt.cmax, sketch1);

        is = jaz::intersection_size(sketch0.begin(), sketch0.end(), sketch1.begin(), sketch1.end());
        double jacc = (1.0 * is) / (sketch0.size() + sketch1.size());

        // calculate semi_global_alignment
        auto sga = sgalign(s0, s1);

        of << cont << " " << jacc << " " << boost::get<0>(sga) << " " << boost::get<1>(sga) << " " << boost::get<2>(sga) << std::endl;
    } // for i

}; // process


int main(int argc, char* argv[]) {
    std::map<std::string, std::string> conf;

    bool res = false;
    int pos = 0;

    boost::tie(res, pos) = jaz::parse_argv(argc, argv, conf);
    if (res == false) return -1;

    // we are using:
    // input
    // output
    // is_dna
    // gaps
    // kmer
    // mod
    // cmax

    AppConfig opt;
    std::string err = "";

    boost::tie(res, err) = opt.set(conf);
    if (res == false) {
	std::cout << "error: " << err << std::endl;
	return -1;
    }

    AppLog log;
    Reporter report(std::cout, std::cout);

    // get input
    SequenceList SL;

    std::ifstream f(opt.input.c_str());
    if (!f) return -1;

    pos = 0;
    bio::fasta_input_iterator<> fi(f), end;

    Sequence s;

    for (; fi != end; ++fi, ++pos) {
	s.id = pos;
	s.s = fi->second;
	SL.seqs.push_back(s);
    }

    SL.N = SL.seqs.size();

    f.close();

    // process
    std::ofstream of(opt.output.c_str());

    // call process with opt, sequence list, and output stream
    // opt stores all variables for a run
    // i.e. kmer, mod, iterations
    process(opt, SL, of);

    of.close();

    return 0;
} // main

/***
 *  $Id$
 **
 *  File: read_lines.hpp
 *  Created: Mar 13, 2017
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017 Jaroslaw Zola
 *  Distributed under the Boost Software License, Version 1.0.
 *  See accompanying file LICENSE_BOOST.txt.
 *
 *  This file is part of mpix2.
 */

#ifndef MPIX2_READ_LINES_HPP
#define MPIX2_READ_LINES_HPP

#include <string>
#include <vector>
#include <mpi.h>


namespace mpix {

  inline bool read_lines(const std::string& name, std::vector<std::string>& seq, MPI_Comm comm) {
      int size, rank;

      MPI_Comm_size(comm, &size);
      MPI_Comm_rank(comm, &rank);

      if (size == 1) return false;

      MPI_File fh;
      int res = MPI_File_open(comm, const_cast<char*>(name.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
      if (res != MPI_SUCCESS) return false;

      MPI_Offset sz;
      MPI_File_get_size(fh, &sz);

      unsigned int bsz = static_cast<unsigned int>((static_cast<double>(sz) / size) + 0.5);
      if (sz <= bsz * (size - 1)) bsz = sz / size;

      // create offset
      unsigned long long int offset = rank * bsz;

      // update block size on last processor
      if (rank == size - 1) bsz = sz - (rank * bsz);

      // let's read
      MPI_Status stat;
      char type[] = "native";

      std::vector<char> buf(bsz);
      int last = bsz - 1;

      MPI_File_set_view(fh, offset, MPI_BYTE, MPI_BYTE, type, MPI_INFO_NULL);
      MPI_File_read_all(fh, reinterpret_cast<char*>(&buf[0]), bsz, MPI_BYTE, &stat);
      MPI_File_close(&fh);

      // find first end of line (which may be needed on predecessor)
      int pos = 0;
      for (; pos < bsz; ++pos) if (buf[pos] == '\n') break;

      // too many processors to split by line
      if ((rank != size - 1) && (pos > last)) return false;

      // if my last char is \n no need for communication
      char sneed = (buf[last] != '\n');
      char rneed = 0;

      const int TAG = 111;

      if (rank == 0) MPI_Send(&sneed, 1, MPI_CHAR, 1, TAG, comm);
      else if (rank == size - 1) MPI_Recv(&rneed, 1, MPI_CHAR, rank - 1, TAG, comm, &stat);
      else MPI_Sendrecv(&sneed, 1, MPI_CHAR, rank + 1, TAG,
                        &rneed, 1, MPI_CHAR, rank - 1, TAG, comm, &stat);

      // error if my neighbor needs data and I have entire line
      if ((rneed == true) && (pos >= last)) return false;

      // let's send buffer size
      int spos = (rneed == true) ? (pos + 1) : 0;
      int rpos = 0;

      if (rank == size - 1) MPI_Send(&spos, 1, MPI_INT, rank - 1, TAG, comm);
      else if (rank == 0) MPI_Recv(&rpos, 1, MPI_INT, 1, TAG, comm, &stat);
      else MPI_Sendrecv(&spos, 1, MPI_INT, rank - 1, TAG,
                        &rpos, 1, MPI_INT, rank + 1, TAG, comm, &stat);

      if (rpos > 0) buf.resize(buf.size() + rpos);

      // a now buffer
      if (rank == size - 1) MPI_Send(&buf[0], spos, MPI_CHAR, rank - 1, TAG, comm);
      else if (rank == 0) MPI_Recv(&buf[bsz], rpos, MPI_CHAR, 1, TAG, comm, &stat);
      else MPI_Sendrecv(&buf[0], spos, MPI_CHAR, rank - 1, TAG,
                        &buf[bsz], rpos, MPI_CHAR, rank + 1, TAG, comm, &stat);

      // let's extract lines now
      seq.clear();

      bsz = buf.size();
      pos = spos;

      for (int i = pos; i < bsz; ++i) {
          if (buf[i] == '\n') {
              seq.push_back(std::string(buf.begin() + pos, buf.begin() + i));
              pos = i + 1;
          }
      }

      if (pos != bsz) seq.push_back(std::string(buf.begin() + pos, buf.begin() + bsz));

      return true;
  } // read_lines

} // namespace mpix

#endif // MPIX2_READ_LINES_HPP

#!/bin/bash

clear

DIR=../../ELaSTIC-`cat VERSION`-mpe

rm -rf build/*
cd build/
MPE=/opt/mpe2-2.4.9b-mpich-gnu
cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR -DWITH_MPE=1 -DMPE_INCLUDE_DIR=$MPE/include -DMPE_LIB_DIR=$MPE/lib
make -j8 install
